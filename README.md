Creating the server key and certificate
keytool -genkeypair -alias serverkey -keyalg RSA -keypass password -keystore server-keystore.jks -storepass password

Creating a client key and certificate
keytool -genkeypair -alias clientkey -keyalg RSA -keypass password -keystore client-keystore.jks -storepass password

Export server certificate to file
keytool -exportcert -alias serverkey -file server-public.cer -keystore server-keystore.jks -storepass password

Export client certificate to file
keytool -exportcert -alias clientkey -file client-public.cer -keystore client-keystore.jks -storepass password

Import server certificate into a separate keystore (client truststore)
keytool -importcert -keystore client-truststore.jks -alias servercert -file server-public.cer -storepass password -noprompt

Importing the client certificate into a separate keystore (server truststore)
keytool -importcert -keystore server-truststore.jks -alias clientcert -file client-public.cer -storepass password -noprompt




mvn clean install

mvn spring-boot:run



