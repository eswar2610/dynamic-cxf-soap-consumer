package com.example.cxf.controller;

import com.example.cxf.config.DynamicClientSelector;
import com.example.cxf.config.KeyStoreProperties;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.endpoint.ClientImpl;
import org.apache.cxf.endpoint.Endpoint;
import org.apache.cxf.service.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.xml.namespace.QName;
import java.util.List;

@Controller
public class ApiController {

    @Autowired
    private DynamicClientSelector dynamicClientExecutor;

    @Autowired
    private KeyStoreProperties keyStoreProperties;


    @ResponseBody
    @GetMapping("/checkServerA")
    public String checkServerA() {

        // Person person = Person.builder().firstname("eswar").username("eswar").lastname("prasad").password("password".toCharArray()).build();


        try {
            keyStoreProperties.setPropsByType("A");
            Client c =  dynamicClientExecutor.createDynamicClient(keyStoreProperties.getEndpoint(),keyStoreProperties);
            ClientImpl clientImpl = (ClientImpl) c;
            Endpoint endpoint = clientImpl.getEndpoint();
            ServiceInfo serviceInfo = endpoint.getService().getServiceInfos().get(0);
            QName bindingName = new QName(keyStoreProperties.getNameSpaceUri(), keyStoreProperties.getLocalPort());
            BindingInfo binding = serviceInfo.getBinding(bindingName);
            QName opName = new QName(keyStoreProperties.getNameSpaceUri(), "GetPersonInfo");
            BindingOperationInfo boi = binding.getOperation(opName);
            BindingMessageInfo inputMessageInfo = boi.getInput();
            List<MessagePartInfo> parts = inputMessageInfo.getMessageParts();
            MessagePartInfo partInfo = parts.get(0);
            Class<?> partClass = partInfo.getTypeClass();
            System.out.println(partClass.getCanonicalName()); // GetPersonInfo
            Object inputObject = partClass.newInstance();

            //-- if method requires params insert it in input obj

            Object[] result = c.invoke(opName, inputObject);
            /*
            Class<?> resultClass = result[0].getClass();
            System.out.println(resultClass.getCanonicalName());
            PropertyDescriptor resultDescriptor = new PropertyDescriptor("agentWSResponse", resultClass);
            Object wsResponse = resultDescriptor.getReadMethod().invoke(result[0]);
            Class<?> wsResponseClass = wsResponse.getClass();
            System.out.println(wsResponseClass.getCanonicalName());
            PropertyDescriptor agentNameDescriptor = new PropertyDescriptor("agentName", wsResponseClass);
            String agentName = (String)agentNameDescriptor.getReadMethod().invoke(wsResponse);
            System.out.println("Agent name: " + agentName);
            */
        } catch (Exception e) {
            e.printStackTrace();
        }

        //personRegistryClient.addPerson(person);

        //   personRegistryClient.getAllPerson().stream().forEach(System.out::println);
        return "working see console for output";
    }

    @ResponseBody
    @GetMapping("/checkServerB")
    public String checkServerB() {

        // Person person = Person.builder().firstname("eswar").username("eswar").lastname("prasad").password("password".toCharArray()).build();


        try {
            keyStoreProperties.setPropsByType("B");
            Client c =  dynamicClientExecutor.createDynamicClient(keyStoreProperties.getEndpoint(),keyStoreProperties);
            ClientImpl clientImpl = (ClientImpl) c;
            Endpoint endpoint = clientImpl.getEndpoint();
            ServiceInfo serviceInfo = endpoint.getService().getServiceInfos().get(0);
            QName bindingName = new QName(keyStoreProperties.getNameSpaceUri(), keyStoreProperties.getLocalPort());
            BindingInfo binding = serviceInfo.getBinding(bindingName);
            QName opName = new QName(keyStoreProperties.getNameSpaceUri(), "GetPersonInfo");
            BindingOperationInfo boi = binding.getOperation(opName);
            BindingMessageInfo inputMessageInfo = boi.getInput();
            List<MessagePartInfo> parts = inputMessageInfo.getMessageParts();
            MessagePartInfo partInfo = parts.get(0);
            Class<?> partClass = partInfo.getTypeClass();
            System.out.println(partClass.getCanonicalName()); // GetPersonInfo
            Object inputObject = partClass.newInstance();

            //-- if method requires params insert it in input obj

            Object[] result = c.invoke(opName, inputObject);
            /*
            Class<?> resultClass = result[0].getClass();
            System.out.println(resultClass.getCanonicalName());
            PropertyDescriptor resultDescriptor = new PropertyDescriptor("agentWSResponse", resultClass);
            Object wsResponse = resultDescriptor.getReadMethod().invoke(result[0]);
            Class<?> wsResponseClass = wsResponse.getClass();
            System.out.println(wsResponseClass.getCanonicalName());
            PropertyDescriptor agentNameDescriptor = new PropertyDescriptor("agentName", wsResponseClass);
            String agentName = (String)agentNameDescriptor.getReadMethod().invoke(wsResponse);
            System.out.println("Agent name: " + agentName);
            */
        } catch (Exception e) {
            e.printStackTrace();
        }

        //personRegistryClient.addPerson(person);

        //   personRegistryClient.getAllPerson().stream().forEach(System.out::println);
        return "working see console for output " ;
    }

}
