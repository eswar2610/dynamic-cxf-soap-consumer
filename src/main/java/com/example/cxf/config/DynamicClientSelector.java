package com.example.cxf.config;

import com.example.cxf.config.ssl.api.SSLContextService;
import lombok.RequiredArgsConstructor;
import org.apache.cxf.Bus;
import org.apache.cxf.BusFactory;
import org.apache.cxf.configuration.jsse.TLSClientParameters;
import org.apache.cxf.configuration.security.FiltersType;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxws.endpoint.dynamic.JaxWsDynamicClientFactory;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transport.http.HTTPConduitConfigurer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
@RequiredArgsConstructor
public class DynamicClientSelector {

    private final SSLContextService sslContextService;



    private static Map<String, Client> clientMap = new ConcurrentHashMap<>();

    public Client createDynamicClient(String wsdlUrl,KeyStoreProperties keyStoreProperties) {
        if (!wsdlUrl.contains("?wsdl")) {
            wsdlUrl = wsdlUrl + "?wsdl";
        }

        if (clientMap.containsKey(wsdlUrl)) {
            return clientMap.get(wsdlUrl);
        }

        BusFactory bf = BusFactory.newInstance();
        Bus bus = bf.createBus();
        bus.getInInterceptors().add(new LoggingInInterceptor());
        bus.getOutInterceptors().add(new LoggingOutInterceptor());
        String finalWsdlUrl = wsdlUrl;
        bus.setExtension(new HTTPConduitConfigurer() {

            @Override
            public void configure(String name, String address, HTTPConduit httpConduit) {
                //set conduit parameters ...
//                httpConduit.getClient().setReceiveTimeout(10000);

                // ex. disable host name verification
                TLSClientParameters tlsClientParameters = new TLSClientParameters();
                tlsClientParameters.setHostnameVerifier(new HostnameVerifier() {
                    @Override
                    public boolean verify(String hostname, SSLSession session) {
                        return true;
                    }
                });

                //keyStoreProperties.setPropsByType(type);
                SSLContext sslContext = sslContextService.initSSLContext(keyStoreProperties);
                tlsClientParameters.setDisableCNCheck(true);
                tlsClientParameters.setSSLSocketFactory(sslContext.getSocketFactory());
                FiltersType filters = new FiltersType();
                filters.getInclude().add(".*_EXPORT_.*");
                filters.getInclude().add(".*_EXPORT1024_.*");
                filters.getInclude().add(".*_WITH_DES_.*");
                filters.getInclude().add(".*_WITH_NULL_.*");
                filters.getInclude().add(".*_DH_anon_.*");
                tlsClientParameters.setCipherSuitesFilter(filters);
                httpConduit.setTlsClientParameters(tlsClientParameters);

            }
        }, HTTPConduitConfigurer.class);

        JaxWsDynamicClientFactory dcf = JaxWsDynamicClientFactory.newInstance(bus);

        Client client = null;


        client = dcf.createClient(wsdlUrl);

        return client;
    }

}
