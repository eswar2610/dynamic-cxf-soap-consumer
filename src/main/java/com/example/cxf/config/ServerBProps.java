package com.example.cxf.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
public class ServerBProps {

    @Value("${server2.ssl.keystore}")
    private String keystoreLocation;

    @Value("${server2.ssl.keystore-pass}")
    private char[] keystorePassword;

    @Value("${server2.ssl.truststore}")
    private String truststoreLocation;

    @Value("${server2.ssl.truststore-pass}")
    private char[] truststorePassword;

    @Value("${server2.ssl.keystore-type}")
    private String keystoreType;

    @Value("${server2.ssl.truststore-type}")
    private String truststoreType;

    @Value("${server2.ssl.keystore-key-pass}")
    private char[] keyManagerPassword;


    @Value("${server2.ssl.trust-all}")
    private boolean isTrustedAll;


    @Value("${server2.endpoint}")
    private String endpoint;

    @Value("${server2.namespace-uri}")
    private String nameSpaceUri;

    @Value("${server2.local-port}")
    private String localPort;

    public String getKeystoreLocation() {
        return keystoreLocation;
    }

    public void setKeystoreLocation(String keystoreLocation) {
        this.keystoreLocation = keystoreLocation;
    }

    public char[] getKeystorePassword() {
        return keystorePassword;
    }

    public void setKeystorePassword(char[] keystorePassword) {
        this.keystorePassword = keystorePassword;
    }

    public String getTruststoreLocation() {
        return truststoreLocation;
    }

    public void setTruststoreLocation(String truststoreLocation) {
        this.truststoreLocation = truststoreLocation;
    }

    public char[] getTruststorePassword() {
        return truststorePassword;
    }

    public void setTruststorePassword(char[] truststorePassword) {
        this.truststorePassword = truststorePassword;
    }

    public String getKeystoreType() {
        return keystoreType;
    }

    public void setKeystoreType(String keystoreType) {
        this.keystoreType = keystoreType;
    }

    public String getTruststoreType() {
        return truststoreType;
    }

    public void setTruststoreType(String truststoreType) {
        this.truststoreType = truststoreType;
    }

    public char[] getKeyManagerPassword() {
        return keyManagerPassword;
    }

    public void setKeyManagerPassword(char[] keyManagerPassword) {
        this.keyManagerPassword = keyManagerPassword;
    }

    public boolean isTrustedAll() {
        return isTrustedAll;
    }

    public void setTrustedAll(boolean trustedAll) {
        isTrustedAll = trustedAll;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getNameSpaceUri() {
        return nameSpaceUri;
    }

    public void setNameSpaceUri(String nameSpaceUri) {
        this.nameSpaceUri = nameSpaceUri;
    }

    public String getLocalPort() {
        return localPort;
    }

    public void setLocalPort(String localPort) {
        this.localPort = localPort;
    }

    @Override
    public String toString() {
        return "ServerBProps{" +
                "keystoreLocation='" + keystoreLocation + '\'' +
                ", keystorePassword=" + Arrays.toString(keystorePassword) +
                ", truststoreLocation='" + truststoreLocation + '\'' +
                ", truststorePassword=" + Arrays.toString(truststorePassword) +
                ", keystoreType='" + keystoreType + '\'' +
                ", truststoreType='" + truststoreType + '\'' +
                ", keyManagerPassword=" + Arrays.toString(keyManagerPassword) +
                ", isTrustedAll=" + isTrustedAll +
                '}';
    }
}
