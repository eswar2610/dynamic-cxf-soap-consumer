package com.example.cxf.config.ssl.api;

import com.example.cxf.config.KeyStoreProperties;

import javax.net.ssl.KeyManager;
import javax.net.ssl.TrustManager;


public interface KeyManagerService {

    KeyManager[] initKeyManagers(KeyStoreProperties keyStoreProperties);
    TrustManager[] initTrustManagers(KeyStoreProperties keyStoreProperties);
}
