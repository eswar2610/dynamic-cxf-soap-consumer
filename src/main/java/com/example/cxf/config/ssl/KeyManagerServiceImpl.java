package com.example.cxf.config.ssl;

import com.example.cxf.config.KeyStoreProperties;
import com.example.cxf.config.ServerAProps;
import com.example.cxf.config.ServerBProps;
import com.example.cxf.config.ssl.api.KeyManagerService;
import com.example.cxf.config.ssl.api.KeyStoreService;
import com.example.cxf.config.ssl.exception.SSLConfigurationRuntimeException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.X509Certificate;


@Component
public class KeyManagerServiceImpl implements KeyManagerService {

    @Autowired
    private ServerAProps serverAProps;

    @Autowired
    private ServerBProps serverBProps;

    private final KeyStoreService keyStoreService;
    // private final char[] keyManagerPassword;
    // private final boolean isTrustedAll;

    @Autowired
    public KeyManagerServiceImpl(KeyStoreService keyStoreService
    ) {
        System.err.println("Initialized... KeyManager");
        this.keyStoreService = keyStoreService;
        /*this.keyManagerPassword = keyManagerPassword;
        this.isTrustedAll = isTrustedAll;*/
    }

    @Override
    public KeyManager[] initKeyManagers(KeyStoreProperties keyStoreProperties) {

        String defaultAlgorithm = KeyManagerFactory.getDefaultAlgorithm();
        try {
            KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(defaultAlgorithm);
            KeyStore keyStore = keyStoreService.initKeyStore(keyStoreProperties);
            keyManagerFactory.init(keyStore, keyStoreProperties.getKeyManagerPassword());
            return keyManagerFactory.getKeyManagers();
        } catch (NoSuchAlgorithmException e) {
            throw new SSLConfigurationRuntimeException("Error while getting instance of KeyManagerFactory: " + e);
        } catch (UnrecoverableKeyException | KeyStoreException e) {
            throw new SSLConfigurationRuntimeException("Error while initializing KeyManagerFactory: " + e);
        }
    }

    @Override
    public TrustManager[] initTrustManagers(KeyStoreProperties keyStoreProperties) {



        return keyStoreProperties.isTrustedAll() ? getTrustAllManagers() : getRealTrustManagers(keyStoreProperties);
    }

    private TrustManager[] getRealTrustManagers(KeyStoreProperties keyStoreProperties) {
        String defaultAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
        try {
            TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(defaultAlgorithm);
            KeyStore trustStore = keyStoreService.initTrustStore(keyStoreProperties);
            trustManagerFactory.init(trustStore);
            return trustManagerFactory.getTrustManagers();
        } catch (NoSuchAlgorithmException e) {
            throw new SSLConfigurationRuntimeException("Error while getting instance of TrustManagerFactory: " + e);
        } catch (KeyStoreException e) {
            throw new SSLConfigurationRuntimeException("Error while initializing TrustManagerFactory: " + e);
        }
    }

    private TrustManager[] getTrustAllManagers() {
        return new TrustManager[]{
                new X509TrustManager() {
                    public X509Certificate[] getAcceptedIssuers() {
                        return new X509Certificate[0];
                    }

                    public void checkClientTrusted(X509Certificate[] certs, String authType) {
                    }

                    public void checkServerTrusted(X509Certificate[] certs, String authType) {
                    }
                }};
    }

}
