package com.example.cxf.config.ssl.api;

import com.example.cxf.config.KeyStoreProperties;

import javax.net.ssl.SSLContext;


public interface SSLContextService {

    SSLContext initSSLContext(KeyStoreProperties keyStoreProperties);
}
