package com.example.cxf.config.ssl;

import com.example.cxf.config.KeyStoreProperties;
import com.example.cxf.config.ssl.api.KeyManagerService;
import com.example.cxf.config.ssl.api.SSLContextService;
import com.example.cxf.config.ssl.exception.SSLConfigurationRuntimeException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;


@Component
@RequiredArgsConstructor
public class SSLContextServiceImpl implements SSLContextService {

    private static final String SSL_PROTOCOL_ALIAS = "TLS";
    private final KeyManagerService keyManagerService;

    @Override
    public SSLContext initSSLContext(KeyStoreProperties keyStoreProperties) {

        try {

            SSLContext sslContext = SSLContext.getInstance(SSL_PROTOCOL_ALIAS);
            KeyManager[] keyManagers = keyManagerService.initKeyManagers(keyStoreProperties);
            TrustManager[] trustManagers = keyManagerService.initTrustManagers(keyStoreProperties);
            sslContext.init(keyManagers, trustManagers, new SecureRandom());
            HttpsURLConnection
                    .setDefaultSSLSocketFactory(sslContext.getSocketFactory());
            return sslContext;
        } catch (NoSuchAlgorithmException e) {
            throw new SSLConfigurationRuntimeException("Error while getting instance of SSLContext: " + e);
        } catch (KeyManagementException e) {
            throw new SSLConfigurationRuntimeException("Error while initializing SSLContext: " + e);
        }
    }
}
