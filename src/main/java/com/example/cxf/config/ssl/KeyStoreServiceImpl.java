package com.example.cxf.config.ssl;

import com.example.cxf.config.*;
import com.example.cxf.config.ServerAProps;
import com.example.cxf.config.ServerBProps;
import com.example.cxf.config.ssl.api.KeyStoreService;
import com.example.cxf.config.ssl.exception.SSLConfigurationRuntimeException;
import com.example.cxf.config.KeyStoreProperties;
import com.google.common.base.Preconditions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;


@Component
public class KeyStoreServiceImpl implements KeyStoreService {

    @Autowired
    private ServerAProps serverAProps;
    @Autowired
    private ServerBProps serverBProps;


    // private final String keystoreLocation;
    //private final char[] keystorePassword;
    //private final String truststoreLocation;
    //private final char[] truststorePassword;
    //private final String keystoreType;
    //private final String truststoreType;

    /*@Autowired
    public KeyStoreServiceImpl(
            @Value("${client.ssl.keystore-pass}") char[] keystorePassword,
            @Value("${client.ssl.truststore}") String truststoreLocation,
            @Value("${client.ssl.truststore-pass}") char[] truststorePassword,
            @Value("${client.ssl.keystore-type}") String keystoreType,
            @Value("${client.ssl.truststore-type}") String truststoreType) {
        System.err.println("Initialized... KeyStore");
        //  this.keystoreLocation = keystoreLocation;
        this.keystorePassword = keystorePassword;
        this.truststoreLocation = truststoreLocation;
        this.truststorePassword = truststorePassword;
        this.keystoreType = keystoreType;
        this.truststoreType = truststoreType;
    }*/

    @Override
    public KeyStore initKeyStore(KeyStoreProperties keyStoreProperties) {
        // String server = "A";
        /*switch (server) {
            case "A":
                return initKeyStore(serverAProps.getKeystoreLocation(), serverAProps.getKeystorePassword(), serverAProps.getKeystoreType());

            case "B":
                return initKeyStore(serverBProps.getKeystoreLocation(), serverBProps.getKeystorePassword(), serverBProps.getKeystoreType());

        }*/

        return initKeyStore(keyStoreProperties.getKeystoreLocation(), keyStoreProperties.getKeystorePassword(), keyStoreProperties.getKeystoreType());
    }

    @Override
    public KeyStore initTrustStore(KeyStoreProperties keyStoreProperties) {


        return initKeyStore(keyStoreProperties.getTruststoreLocation(), keyStoreProperties.getTruststorePassword(), keyStoreProperties.getTruststoreType());
    }

    private KeyStore initKeyStore(String keystoreLocation, char[] password, String keystoreType) {
        Preconditions.checkNotNull(keystoreLocation, "KeyStore location cannot be null!");
        Preconditions.checkNotNull(password, "KeyStore password cannot be null!");

        try {
            KeyStore keyStore = KeyStore.getInstance(keystoreType);
            keyStore.load(ClasspathFileLoader.loadFile(keystoreLocation), password);
            return keyStore;
        } catch (IOException e) {
            throw new SSLConfigurationRuntimeException("Error while getting keystore from file path: " + e);
        } catch (CertificateException | NoSuchAlgorithmException | KeyStoreException e) {
            throw new SSLConfigurationRuntimeException("Error while getting instance of keystore: " + e);
        }
    }
}
