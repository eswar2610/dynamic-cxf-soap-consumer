package com.example.cxf.config.ssl.api;

import com.example.cxf.config.KeyStoreProperties;

import java.security.KeyStore;


public interface KeyStoreService {

    KeyStore initKeyStore(KeyStoreProperties keyStoreProperties);
    KeyStore initTrustStore(KeyStoreProperties keyStoreProperties);
}
