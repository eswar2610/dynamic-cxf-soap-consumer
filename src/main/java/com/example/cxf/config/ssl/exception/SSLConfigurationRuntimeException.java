package com.example.cxf.config.ssl.exception;


public class SSLConfigurationRuntimeException extends RuntimeException {

    public SSLConfigurationRuntimeException(String message) {
        super(message);
    }
}
