package com.example.cxf.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class KeyStoreProperties {


    @Autowired
    private ServerBProps serverBProps;

    @Autowired
    private ServerAProps serverAProps;

    private String keystoreLocation;


    private char[] keystorePassword;


    private String truststoreLocation;


    private char[] truststorePassword;


    private String keystoreType;


    private String truststoreType;


    private char[] keyManagerPassword;


    private boolean isTrustedAll;


    private String endpoint;


    private String nameSpaceUri;


    private String localPort;

    public void setPropsByType(String type) {
        switch (type) {
            case "A":
                // return initKeyStore(serverAProps.getKeystoreLocation(), serverAProps.getKeystorePassword(), serverAProps.getKeystoreType());


                keystoreLocation = serverAProps.getKeystoreLocation();
                keystorePassword = serverAProps.getKeystorePassword();
                truststoreLocation = serverAProps.getTruststoreLocation();
                truststorePassword = serverAProps.getTruststorePassword();
                keystoreType = serverAProps.getKeystoreType();
                truststoreType = serverAProps.getTruststoreType();
                keyManagerPassword = serverAProps.getKeyManagerPassword();
                isTrustedAll = serverAProps.isTrustedAll();
                endpoint = serverAProps.getEndpoint();
                nameSpaceUri = serverAProps.getNameSpaceUri();
                localPort = serverAProps.getLocalPort();
                break;
            case "B":
                // return initKeyStore(serverBProps.getKeystoreLocation(), serverBProps.getKeystorePassword(), serverBProps.getKeystoreType());
                keystoreLocation = serverBProps.getKeystoreLocation();
                keystorePassword = serverBProps.getKeystorePassword();
                truststoreLocation = serverBProps.getTruststoreLocation();
                truststorePassword = serverBProps.getTruststorePassword();
                keystoreType = serverBProps.getKeystoreType();
                truststoreType = serverBProps.getTruststoreType();
                keyManagerPassword = serverBProps.getKeyManagerPassword();
                isTrustedAll = serverBProps.isTrustedAll();
                endpoint = serverBProps.getEndpoint();
                nameSpaceUri = serverBProps.getNameSpaceUri();
                localPort = serverBProps.getLocalPort();
                break;
        }
    }

    public String getKeystoreLocation() {
        return keystoreLocation;
    }

    public void setKeystoreLocation(String keystoreLocation) {
        this.keystoreLocation = keystoreLocation;
    }

    public char[] getKeystorePassword() {
        return keystorePassword;
    }

    public void setKeystorePassword(char[] keystorePassword) {
        this.keystorePassword = keystorePassword;
    }

    public String getTruststoreLocation() {
        return truststoreLocation;
    }

    public void setTruststoreLocation(String truststoreLocation) {
        this.truststoreLocation = truststoreLocation;
    }

    public char[] getTruststorePassword() {
        return truststorePassword;
    }

    public void setTruststorePassword(char[] truststorePassword) {
        this.truststorePassword = truststorePassword;
    }

    public String getKeystoreType() {
        return keystoreType;
    }

    public void setKeystoreType(String keystoreType) {
        this.keystoreType = keystoreType;
    }

    public String getTruststoreType() {
        return truststoreType;
    }

    public void setTruststoreType(String truststoreType) {
        this.truststoreType = truststoreType;
    }

    public char[] getKeyManagerPassword() {
        return keyManagerPassword;
    }

    public void setKeyManagerPassword(char[] keyManagerPassword) {
        this.keyManagerPassword = keyManagerPassword;
    }

    public boolean isTrustedAll() {
        return isTrustedAll;
    }

    public void setTrustedAll(boolean trustedAll) {
        isTrustedAll = trustedAll;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getNameSpaceUri() {
        return nameSpaceUri;
    }

    public void setNameSpaceUri(String nameSpaceUri) {
        this.nameSpaceUri = nameSpaceUri;
    }

    public String getLocalPort() {
        return localPort;
    }

    public void setLocalPort(String localPort) {
        this.localPort = localPort;
    }
}
